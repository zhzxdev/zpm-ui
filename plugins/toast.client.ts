import { Plugin } from '@nuxt/types'
import { AxiosError } from 'axios'
import iziToast, { IziToastSettings } from 'izitoast'
import 'izitoast/dist/css/iziToast.css'

function isAxiosError(e: any): e is AxiosError {
  return e.isAxiosError === true
}

const toast = {
  ...iziToast,
  $error(e: Error | Record<string, any>) {
    if (isAxiosError(e)) {
      const message = e.response?.data?.message ?? '发生了错误'
      if (e.response?.status === 403 && e.response.data.message === 'Invalid token') {
        iziToast.error({ title: '会话过期', message: '请重新登录' })
        return
      }
      iziToast.error({ title: '失败', message })
      return
    }
    iziToast.error({ title: '错误', message: e.message })
  },
  async $wrap(fn: () => Promise<void | string | IziToastSettings>) {
    try {
      const r = await fn()
      if (typeof r === 'string') {
        this.success({ title: '成功', message: r })
      } else if (typeof r === 'object') {
        this.success(r)
      }
    } catch (e) {
      this.$error(e)
    }
  }
}

type Toast = typeof toast

declare module 'vue/types/vue' {
  interface Vue {
    $toast: Toast
  }
}

declare module '@nuxt/types' {
  interface Context {
    $toast: Toast
  }
}

const plugin: Plugin = (_ctx, inject) => {
  inject('toast', toast)
}

export default plugin
