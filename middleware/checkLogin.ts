import { Middleware } from '@nuxt/types'

const middleware: Middleware = ({ store, redirect }) => {
  if (!store.state.login) {
    redirect('/login')
  }
}

export default middleware
