import { NuxtConfig } from '@nuxt/types'
import MonacoWebpackPlugin from 'monaco-editor-webpack-plugin'

const config: NuxtConfig = {
  ssr: false,
  target: 'static',
  head: {
    titleTemplate: '%s - 镇海中学打印机管理系统',
    title: '首页',
    htmlAttrs: {
      lang: 'zh-Hans'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  css: [
    //
    'roboto-fontface/css/roboto/roboto-fontface.css',
    '@mdi/font/css/materialdesignicons.css',
    '~/styles/main.scss'
  ],
  router: {
    mode: 'hash'
  },
  plugins: [
    //
    '~/plugins/toast.client.ts'
  ],
  buildModules: [
    //
    '@nuxt/typescript-build',
    '@nuxtjs/vuetify'
  ],
  modules: [
    //
    '@nuxtjs/proxy',
    '@nuxtjs/axios'
  ],
  axios: {
    baseURL: '/api',
    progress: false
  },
  proxy: ['http://localhost:8012/api'],
  vuetify: {
    customVariables: ['~/styles/variables.scss'],
    optionsPath: '~/vuetify.config.js',
    defaultAssets: false
  },
  build: {
    plugins: [new MonacoWebpackPlugin()]
  }
}

export default config
