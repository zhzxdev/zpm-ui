export function isElectron() {
  return 'require' in window
}

export function getIPC() {
  const { ipcRenderer } = window.require('electron')
  return ipcRenderer
}

export async function invoke(channel: string, ...args: any[]) {
  const ipc = getIPC()
  return await ipc.invoke(channel, ...args)
}

export async function safeInvoke(channel: string, ...args: any[]) {
  try {
    return await invoke(channel, ...args)
  } catch (e) {}
}
