import { isElectron, safeInvoke } from './electron'
import pkg from '~/package.json'

export const APP_VERSION = pkg.version

export function saveToken(token: string) {
  if (isElectron()) {
    safeInvoke('saveToken', token)
  } else {
    const mime = 'application/octet-stream'
    const blob = new Blob([token], { type: mime })
    const a = document.createElement('a')
    a.download = 'zhzx-print-station'
    a.href = URL.createObjectURL(blob)
    a.dataset.downloadurl = [mime, a.download, a.href].join(':')
    a.style.display = 'none'
    document.body.appendChild(a)
    a.click()
    document.body.removeChild(a)
  }
}
