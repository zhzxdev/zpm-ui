export function getUserType(level: number) {
  switch (level) {
    case 0:
      return '普通用户'
    case 1:
      return '学生管理员'
    case 2:
      return '教师管理员'
    case 127:
      return '开发者'
    default:
      return '内置用户'
  }
}
