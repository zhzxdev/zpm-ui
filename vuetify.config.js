import zhHans from 'vuetify/es5/locale/zh-Hans'

export default {
  lang: {
    locales: { zhHans },
    current: 'zhHans'
  }
}
